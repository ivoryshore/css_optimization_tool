Gem::Specification.new do |s|
  s.name = 'css_optimization_tool'
  s.version = '0.0.1'
  s.executables << 'css_optimization_tool'
  s.date = '2014-04-15'
  s.summary = "The CSS optimization tool requests your website and compares the HTML element structure with the CSS rules given. Ways to optimize the CSS to provide more efficient access is then given."
  s.authors = ["Wil Hunt"]
  s.email = "huntwj@gmail.com"
  s.files = ["lib/css_optimization_tool.rb"]
  s.homepage = 'http://rubygems.org/gems/css_optimization_tool'
  s.license = 'MIT'
end